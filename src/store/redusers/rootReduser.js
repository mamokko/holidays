import {
    OPEN_HANDLER,
    CLOSE_HANDLER,
    FETCH_ERROR,
    EMPLOYEE_ITEM,
    OPEN_MODAL,
    CLOSE_MODAL,
    HOLIDAYS_NULL,
    PROJECTS_LIST,
    CLEAR_ARR,
    ARR_NEW,
    IS_VALID,
} from '../actions/actionType';

const initialState = {
    employees: [],
    employee: {},
    open: false,
    isFormValid: false,
    isredirect: false,
    id: null,
    employeeItem: null,
    loading: true,
    error: null,
    modal: false,
    holidays: null,
    projectsList: null,
    clearState: false,
    isValidData: true
};

export default function rootReduser(state = initialState, action) {
    switch(action.type) {
        case OPEN_HANDLER:
            return { ...state, open: true };
        case CLOSE_HANDLER:
            return { ...state, open: false };
        case FETCH_ERROR:
            return { ...state, error: action.error };
        case EMPLOYEE_ITEM:
            return { ...state, employeeItem: action.item, holidays: action.holidays, loading: false };
        case OPEN_MODAL:
            return { ...state, modal: true };
        case CLOSE_MODAL:
            return { ...state, modal: false };
        case HOLIDAYS_NULL:
            return { ...state, holidays: null};
        case PROJECTS_LIST:
            return { ...state, projectsList: action.item};
        case CLEAR_ARR:
            return { ...state, clearState: true };
        case ARR_NEW:
            return { ...state, clearState: false };
        case IS_VALID:
            return { ...state, isValidData: action.data };
        default:
            return state
    }
}