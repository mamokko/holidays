import {
    OPEN_HANDLER,
    CLOSE_HANDLER,
    FETCH_ERROR,
    EMPLOYEE_ITEM,
    OPEN_MODAL,
    CLOSE_MODAL,
    HOLIDAYS_NULL,
    PROJECTS_LIST,
    CLEAR_ARR,
    ARR_NEW,
    IS_VALID,
} from './actionType';
import axios from '../../axios/axios';

export function openHandler() {
    return {
        type: OPEN_HANDLER
    }
}
export function closeHandler() {
    return {
        type: CLOSE_HANDLER
    }
}

export function fetchError(e) {
    return {
        type: FETCH_ERROR,
        error: e
    }
}
export function employeeItem(item, holidays) {
    return {
        type: EMPLOYEE_ITEM,
        item, holidays
    }
}
export function openModal() {
    return {
        type: OPEN_MODAL,
    }
}
export function closeModal() {
    return {
        type: CLOSE_MODAL
    }
}
export function holidaysNull() {
    return {
        type: HOLIDAYS_NULL
    }
}

export  function projectsList(item) {
    return {
        type: PROJECTS_LIST,
        item
    }
}
export function clearArr() {
    return {
        type: CLEAR_ARR
    }
}
export function arrNew() {
    return {
        type: ARR_NEW
    }
}

export function isValid(data) {
    return {
        type: IS_VALID,
        data
    };

}

export function fetchEmployeeItem(id) {
    return async dispatch =>{
        try {
            const response = await axios.get('/employee/' + id + '.json');
            const item = response.data;
            const holidays = response.data.holidays;

            dispatch(employeeItem(item, holidays))
        }  catch (e) {
            dispatch(fetchError(e))
        }
    }
}
export function fetchAllProjects() {
    return async dispatch => {
        try {
            const response = await axios.get('/project.json');
            const item = response.data;

            dispatch(projectsList(item))
        } catch (e) {
            dispatch(fetchError(e))
        }

    }
}
