import React, {Component} from 'react';
import {Switch, Route, Redirect}  from 'react-router-dom';
import './App.css';
import Navigation from './components/Navigation';
import EmployeeList from './containers/EmployeeList';
import Employee from './containers/Employee';
import SummaryCalendar from './containers/SummaryCalendar';
import Directory from './components/Directory';
import Auth from './components/Auth';
import {connect} from "react-redux";



class App extends Component {

   render() {

       return (
           <div>
               {
                   this.props.isValidData === false
                       ?
                       <div className="App">
                           <div style={{width: '1024px', margin: 'auto'}}>
                               <Switch>
                                   <Route path='/auth' component={Auth} />
                                   <Redirect to='/auth' />
                               </Switch>
                           </div>
                       </div>
                       :
                       <div className="App">
                           <Navigation />
                           <div style={{width: '1024px', margin: 'auto'}}>
                               <Switch>
                                   <Route path='/' component={EmployeeList} exact />
                                   <Route path='/employee/:id' component={Employee} />
                                   <Route path='/summaryCalendar' component={SummaryCalendar} />
                                   <Route path='/directory' component={Directory} />
                                   <Redirect to='/' />
                               </Switch>
                           </div>
                       </div>
               }
           </div>
       )
   }

}

function mapStateToProps(state) {
    return {
        isValidData: state.isValidData,
    }
}

export default connect(mapStateToProps)(App);

