import axios from 'axios';

export default axios.create({
    baseURL: 'https://holidays-f4815.firebaseio.com/'
})