import {createMuiTheme} from "@material-ui/core/styles/index";


export const MyTheme = createMuiTheme({
    palette: {
        primary: {
            light: '#339388',
            main: '#00796B',
            dark: '#00544A',
            contrastText: '#fff',
        },
        secondary: {
            light: '#87F3BE',
            main: '#69F0AE',
            dark: '#49A879',
            contrastText: '#fff',
        },
    },
});