import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import './index.css';
import App from './App';
import rootReduser from './store/redusers/rootReduser';
import * as serviceWorker from './serviceWorker';
import {MuiThemeProvider} from "@material-ui/core/styles/index";
import {MyTheme} from './myTheme';
// applyMiddleware
import firebase from 'firebase/app'

var firebaseConfig = {
    apiKey: "AIzaSyAJ4g07Gvl04xtWLJkO6UqR3p5Omv0ZV-g",
    authDomain: "holidays-f4815.firebaseapp.com",
    databaseURL: "https://holidays-f4815.firebaseio.com",
    projectId: "holidays-f4815",
    storageBucket: "holidays-f4815.appspot.com",
    messagingSenderId: "474486967198",
    appId: "1:474486967198:web:ba13e8ec3e1c9fd5"
};

firebase.initializeApp(firebaseConfig);

const store = createStore(rootReduser, applyMiddleware(thunk));

const app = (
    <MuiThemeProvider theme={MyTheme}>
        <Provider store={store} >
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </Provider>
    </MuiThemeProvider>
);

ReactDOM.render(app, document.getElementById('root'));

serviceWorker.unregister();
