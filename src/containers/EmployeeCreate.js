import React, {useState} from 'react';
import useForm from "react-hook-form";
import Button from '@material-ui/core/Button';
import '../App.css';
import DefaultInput from "../components/UI/Input";
import {IMaskInput} from "react-imask";
import Select from "@material-ui/core/Select";
import axios from "../axios/axios";

const EmployeeCreate = (props) => {

    const {register, handleSubmit, errors} = useForm();
    const [departmentList] = useState(props.departmentList);
    const [postList] = useState(props.postList);
    const [open, setOpen] = useState(false);

    const onSubmit = async (data) => {
        try {
            await axios.post('/employee.json', data);
            setOpen(false);
            props.update();
        } catch(e) {
            console.log(e)
        }
    };
    const renderPostItem = () => {
        return postList.map((item, index) => {
            return(
                <option key={index}>
                    {item.name}
                </option>
            )
        })
    };
    const renderDepartmentItem = () => {
        return departmentList.map((item, index) => {
            return(
                <option key={index}>
                    {item.name}
                </option>
            )
        })
    };

    return(
        <React.Fragment>
            <Button
                variant='contained'
                color='primary'
                onClick={() => setOpen(true)}
            >
                Добавить сотрудника
            </Button>
            {
                open
                    ?
                    <form className='createEmployeeForm' onSubmit={handleSubmit(onSubmit)}>

                        <DefaultInput  label='Фамилия' ref={register({required: true})} name='lastName' />
                        {errors.lastName && <p>Это поле не должно быть пустым</p>}

                        <DefaultInput  label='Имя' ref={register({required: true})} name='name' />
                        {errors.name && <p>Это поле не должно быть пустым</p>}

                        <DefaultInput  label='Отчество' ref={register({required: true})} name='patronymic' />
                        {errors.patronymic && <p>Это поле не должно быть пустым</p>}

                        <div className='IMaskInputWrapper'>
                            <label className='label mb25'>Контактный телефон</label>
                            <IMaskInput className='IMaskInput' mask='+{7}(000)000-00-00' inputRef={register({required: true, minLength: 16})} name='phone' />
                        </div>
                        {errors.phone && errors.phone.type === 'required' && <p>Это поле не должно быть пустым</p>}
                        {errors.phone && errors.phone.type === 'minLength' && <p>Неверно введенное количество цифр </p>}

                        <Select name='post' native style={{width: '100%', marginTop: '40px'}} inputRef={register({required: true})}>
                            <option value='' hidden>Должность</option>
                            { postList ? renderPostItem() : null }
                        </Select>
                        {errors.post && <p>Это поле не должно быть пустым</p>}

                        <Select name='department' native style={{width: '100%', marginTop: '40px'}} inputRef={register({required: true})}>
                            <option value='' hidden>Отдел</option>
                            { departmentList ? renderDepartmentItem() : null }
                        </Select>
                        {errors.department && <p>Это поле не должно быть пустым</p>}

                        <div className='flexCenter mt30'>
                            <Button variant='contained' type='submit' color='primary' style={{marginRight: 20}}>
                                Сохранить
                            </Button>
                            <Button variant='contained' color='primary'
                                    onClick={() => setOpen(false)}
                            >
                                Отмена
                            </Button>
                        </div>

                    </form>
                    : null
            }
        </React.Fragment>

    )
};

export default EmployeeCreate;