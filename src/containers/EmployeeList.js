import React, {useEffect, useState} from 'react';
import { Redirect } from 'react-router-dom';
import '../App.css';
import Select from '@material-ui/core/Select';
import axios from "../axios/axios";
import sortArr from '../components/function/arrSort';
import EmployeeCreate from "./EmployeeCreate";


export default function EmployeeList() {

    const [departmentList, setDepartmentList] = useState([]);
    const [postList, setPostList] = useState([]);
    const [employeeList, setEmployeeList] = useState([]);
    const [redirect, setRedirect] = useState(false);
    const [id, setId] = useState(null);

    async function employees() {
        try {
            const response = await axios.get('/employee.json');
            const responseDepartment = await axios.get('/department.json');
            const responsePost = await axios.get('/postList.json');
            setDepartmentList(sortArr(responseDepartment.data, 'name'));
            setPostList(sortArr(responsePost.data, 'name'));

            const employees = Object.keys(response.data).map((key) => {
                const item = response.data[key];
                return {
                    key: key,
                    name: `${item.lastName} ${item.name}`,
                };
            });

            setEmployeeList(sortArr(employees, 'name'));

        } catch (e) { console.log(e) }
    }

    useEffect(() => {employees()}, []);

    const onLink = (event) => {
        const id = event.target.value;
       setRedirect(true);
       setId(id);
    };

    function renderItem() {
        return employeeList.map((item) => {
            return(
                <option
                    key={item.key}
                    value={item.key}
                >
                    {item.name}
                </option>
            );
        })
    };

    return(
        <div>
            {redirect ? <Redirect to={'/employee/' + id} /> : null}
            <h1 className='caption'>Список сотрудников</h1>
            <hr />

            <Select
                className='mr30'
                native
                onChange={onLink}
            >
                <option hidden>Выберите сотрудника</option>
                { employeeList ? renderItem() : null }
            </Select>

            {departmentList && postList
                ? <EmployeeCreate
                    departmentList={departmentList}
                    postList={postList}
                    update={employees}
                />
                : null
            }

        </div>
    )
}