import React, {useEffect, useState} from 'react';
import axios from "../axios/axios";
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import '../App.css';
import arrSort from '../components/function/arrSort';
import SectionItem from "../components/UI/SectionItem";

const SectionList = (props) => {

    const [section,setSection] = useState(null);
    const [open, setOpen] = useState(false);
    const [change, setChange] = useState(null);

    async function fetchDepartmentList() {
        try {
            const response = await axios.get(`/${props.name}.json`);
            const data = Object.keys(response.data).map((key) => {
                const item = response.data[key];
                return {
                    key: key,
                    name: item.name,
                };
            });
            setSection(() => arrSort(data, 'name'));
        } catch(e) {
            console.log(e)
        }
    }

    useEffect(() => {fetchDepartmentList()});

    const submitHandler = (event) => event.preventDefault();

    const onChangeHandler = (event) => {
        const change = {name: event.target.value};
        setChange(change);
    };

    const addDepartment = async() => {
        try {
            await axios.post(`/${props.name}.json`, change);
            setChange(null)
        }  catch (e) {
            console.log(e)
        }
        setOpen(false);
    };
    const renderDepartments = () => {
        return section.map((item) => {
            return(
                <li key={item.key} className='directoryLi'>
                    <SectionItem
                        id={item.key}
                        item={item}
                        name={props.name}
                        update={fetchDepartmentList}
                    />
                </li>
            )
        })
    };


  return(
      <div className='directoryBlock'>
          <h2 className='caption'>{props.label}</h2>
          <ul>
              { section ? renderDepartments() : null }
          </ul>
          <Button
              variant='contained'
              color='primary'
              onClick={() =>setOpen(true)}
          >
              Добавить отдел
          </Button>
          {open
              ? <form onSubmit={submitHandler} className='mt10'>
                  <Input
                      id='createDepartment'
                      defaultValue=''
                      onChange={(event) => onChangeHandler(event)}
                  />
                  <div className='mt10'>
                      <Button
                          size='small'
                          variant="outlined"
                          color='primary'
                          onClick={addDepartment}
                          style={{marginRight: 15}}
                      >
                          Сохранить
                      </Button>
                      <Button
                          size='small'
                          variant="outlined"
                          color='secondary'
                          onClick={() => setOpen(false)}
                      >
                          Отмена
                      </Button>
                  </div>

              </form>
              : null
          }
      </div>
  )
};

export default SectionList;
