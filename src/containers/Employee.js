import React, {useEffect, useState} from 'react';
import Loader from '../components/UI/Loader';
import {Redirect} from "react-router";
import '../App.css';
import TabsMenu from "../components/Tabs";
import Icon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";
import ModalEditCard from "../components/ModalEditCard";
import axios from "../axios/axios";
import arrSort from "../components/function/arrSort";
import * as firebase from "firebase";


const Employee = (props) => {

    const id = props.match.params.id;
    const [employee, setEmployee] = useState(null);
    const [holidayList, setHolidayList] = useState(null);
    const [departmentList, setDepartmentList] = useState(null);
    const [postList, setPostList] = useState(null);
    const [redirect, setRedirect] = useState(false);

    async function receivingData() {
        try {
            const response = await axios.get('/employee/' + id + '.json');
            setEmployee(response.data);
            setHolidayList(response.data.holidays);
        } catch(e) {
            console.log(e)
        }
    }
    async function directoryList() {
        try {
            const response = await axios.get('/department.json');
            const report = await axios.get('/postList.json');

            setDepartmentList(arrSort(response.data, 'name'));
            setPostList(arrSort(report.data, 'name'));
        } catch(e) {
            console.log(e)
        }
    }

    useEffect(() => {
        receivingData();
        directoryList();
    }, []);

    const deleteHandler = () => {
        firebase.database().ref('/employee/' + id).remove()
            .then(() => {
                setRedirect(true)
            })
    };

    return(
        <div className='containerEmployee'>
            {redirect ? <Redirect to={'/'} /> : null}
            { employee && postList && departmentList
                ?
                    <React.Fragment>

                        <div style={{display: 'flex', justifyContent: 'space-between', borderBottom: '1px solid #00544A', backgroundColor: '#87f3be'}}>
                            <p className='employeeHeader'>{employee.lastName + ' ' + employee.name}</p>
                            <div style={{display: 'flex'}}>
                                <ModalEditCard
                                    id={id}
                                    employeeItem={employee}
                                    update={receivingData}
                                    departmentList={departmentList}
                                    postList={postList}
                                />

                                <IconButton
                                    onClick={deleteHandler}
                                    color='primary'
                                >
                                    <Icon>delete</Icon>
                                </IconButton>
                            </div>

                        </div>

                        <div className='containerEmployeeInformation'>
                            <div className='employeeAvatar'></div>
                            <div className='employeeBlock'>
                                <div className='employeeBlockWrapper'>
                                    <span><strong>Фамилия: </strong></span><span>{employee.lastName}</span>
                                </div>
                                <div className='employeeBlockWrapper'>
                                    <span><strong>Имя: </strong></span><span>{employee.name}</span>
                                </div>
                                <div className='employeeBlockWrapper'>
                                    <span><strong>Отчество: </strong></span><span>{employee.patronymic}</span>
                                </div>
                            </div>
                            <div className='employeeBlock'>
                                <div className='employeeBlockWrapper'>
                                    <span><strong>Отдел: </strong></span><span>{employee.department || 'нет'}</span>
                                </div>
                                <div className='employeeBlockWrapper'>
                                    <span><strong>Должность: </strong></span><span>{employee.post || 'нет'}</span>
                                </div>
                                <div className='employeeBlockWrapper'>
                                    <span><strong>Телефон: </strong></span><span>{employee.phone || 'нет'}</span>
                                </div>
                            </div>
                        </div>

                        <TabsMenu id={id} holidayList={holidayList} update={receivingData} />

                    </React.Fragment>
                    : <Loader />
            }

        </div>
    )
};

export default Employee;
