import React, {useEffect, useState} from 'react';
import axios from "../axios/axios";
import 'react-day-picker/lib/style.css';
import moment from 'moment';
import 'moment/locale/ru';
import arrSort from '../components/function/arrSort';
import calcDays from "../components/function/calcDays";
import renderName from "../components/function/renderName";
import renderBody from "../components/function/renderBody";
import '../App.css';
import FilterTime from "../components/FilterTime";
import FilterDepartment from "../components/FilterDepartment";
import FilterProject from "../components/FilterProject";
import CheckFilters from "../components/CheckFilters";

export default function SummaryCalendar() {

    const [newEmp, setNewEmp] = useState(null);
    const [employees, setEmployees] = useState(null);
    const [departments, setDepartments] = useState(null);
    const [projects, setProjects] = useState(null);
    const [startSchedulerDate, setStartSchedulerDate] = useState(moment());
    const [endSchedulerDate, setEndSchedulerDate] = useState(moment().add(100, 'days'));
    const [filterStartDate, setFilterStartDate] = useState('');
    const [filterEndDate, setFilterEndDate] = useState('');
    const [selectDepartment, setSelectDepartment] = useState('Все');
    const [selectProject, setSelectProject] = useState('Все');


    async function receivingData() {
        try {
            const response = await axios.get('/employee.json');
            const responseDepartment = await axios.get('/department.json');
            const responseProjects = await axios.get('/project.json');
            setProjects(responseProjects.data);
            setDepartments(responseDepartment.data);
            setEmployees(arrSort(response.data, 'lastName'));
        } catch(e) {
            console.log(e)
        }
    };

    useEffect(() => {receivingData()}, []);

    function renderMonth() {
        return calcDays(startSchedulerDate, endSchedulerDate).map((item, index) => {
            return <th key={index} colSpan={item.days}>{item.month}</th>
        })
    };
    function renderDays() {
        return calcDays(startSchedulerDate, endSchedulerDate).map((item, i) => {
            return (
                <React.Fragment key={i}>
                    {Array.from({length: item.days}).map((_, idx) =>
                        <th key={idx}>
                            {idx + item.start || null}
                        </th>)}
                </React.Fragment>
            );
        })
    };
    function renderDaysWeek() {
        let weekDays = [];
        for(let day = 0; day <= moment(endSchedulerDate).diff(moment(startSchedulerDate), 'day'); day++) {
            let weekDay = moment(startSchedulerDate).add(day, 'days');
            weekDays.push(
                <th key={day}>
                    {weekDay.format('ddd')}
                </th>
            )
        }
        return weekDays;
    };
    function isHoliday(date, holidays) {
        let holidayFlag = false;
        if(!holidays) {
            return holidayFlag;
        }
        Object.keys(holidays).forEach((item) => {
            const holiday = holidays[item];

            if(date.isBetween(holiday.selectedStartDay, holiday.selectedEndDay, 'day', '[]')) {
                holidayFlag = true;
            }
        });

        return holidayFlag;
    }
    function renderColorDays(employee) {
        let days = [];
        for(let currentDayNumber = 0; currentDayNumber <= moment(endSchedulerDate).diff(moment(startSchedulerDate), 'days'); currentDayNumber++) {
            let currentDay = moment(startSchedulerDate).add(currentDayNumber, 'days');

            if(isHoliday(currentDay, employee.holidays)) {
                days.push(
                    <td className='yesHoliday' key={currentDayNumber}>
                        &nbsp;
                    </td>
                );
            } else {
                days.push(
                    <td className='noHoliday' key={currentDayNumber}>
                        &nbsp;
                    </td>
                );
            }
        }
        return days;
    };

    const handleStartDayChange = (day) => { setFilterStartDate(day)};
    const handleEndDayChange = (day) => {setFilterEndDate(day)};

    const renderThisDates = () => {
        setStartSchedulerDate(filterStartDate);
        setEndSchedulerDate(filterEndDate);
    };
    const renderThisYear = () => {
        setStartSchedulerDate(moment().dayOfYear(1));
        setEndSchedulerDate(moment().dayOfYear(364));
    };
    const renderThisMonth = () => {
        const monthDays = moment().daysInMonth();
        setStartSchedulerDate(moment().date(1));
        setEndSchedulerDate(moment().date(monthDays));
    };

    const onChangeHandlerDepartment = event => {setSelectDepartment(event.target.value)};
    const onChangeHandlerProject = event => {setSelectProject(event.target.value)};

    const resetDepartment = () => {setSelectDepartment('Все')};
    const resetProject = () => {setSelectProject('Все')};
    const resetTime = () => {
        setStartSchedulerDate( moment());
        setEndSchedulerDate(moment().add(100, 'days'));
    };

    return(
        <div>
            <div className='summaryCalendar__container'>
                <FilterTime
                    startDayChange={handleStartDayChange}
                    endDayChange={handleEndDayChange}
                    renderThisDates={renderThisDates}
                    renderThisYear={renderThisYear}
                    renderThisMonth={renderThisMonth}
                />
                <FilterDepartment
                    selectDepartment={selectDepartment}
                    changeDepartment={onChangeHandlerDepartment}
                    departments={departments}
                />
                <FilterProject
                    selectProject={selectProject}
                    changeProject={onChangeHandlerProject}
                    projects={projects}
                />
            </div>
            <div>
                <h1 className='caption'>Таблица-Календарь</h1>
                <CheckFilters
                    selectDepartment={selectDepartment}
                    selectProject={selectProject}
                    resetTime={resetTime}
                    resetDepartment={resetDepartment}
                    resetProject={resetProject}
                />

                {
                    employees
                        ?
                        <div className='containerCalender'>
                            <div>
                                <div  className='captionTable'><span>Сотрудник</span></div>
                                {renderName(selectDepartment, selectProject, employees)}
                            </div>
                            <div className='tableCalendar'>
                                <table>
                                    <thead>
                                    <tr>{renderMonth()}</tr>
                                    <tr>{renderDays()}</tr>
                                    <tr>{renderDaysWeek()}</tr>
                                    </thead>
                                    <tbody>
                                    {renderBody(selectDepartment, selectProject, employees, renderColorDays)}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        : null
                }
            </div>
        </div>
    )
}
