import React, {useState} from 'react';
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import useForm from "react-hook-form";
import axios from "../axios/axios";

const AddNewProjectInMyCard = (props) => {

    const {register, handleSubmit, errors} = useForm();
    const [open, setOpen] = useState(false);

    const onSubmit = async (data) => {
        try {
            await axios.post('/employee/' + props.id + '/project.json', data);
            props.update(props.id);
            setOpen(false);
        } catch(e) {
            console.log(e)
        }
    };
    const renderItem = () => {
        return props.projectsList.map((item, i) => {
            return(
                <option key={i}>{item.name}</option>
            )
        })
    };

    return (
        <div className='containerAddProjectMyCard'>
            <Button variant='contained' color='primary' onClick={() => setOpen(true)}>
                Добавить проект
            </Button>
            {
                open
                    ?
                    <form className='formAddProjectMyCard' onSubmit={handleSubmit(onSubmit)}>
                        <div>
                            <Select className='formAddProjectMyCardSelect' name='nameProject' native inputRef={register({required: true})}>
                                <option value='' hidden>Выберите проект</option>
                                { renderItem() }
                            </Select>
                            {errors.nameProject && <p>Это поле не должно быть пустым</p>}
                        </div>

                        <div className='formAddProjectMyCardButtonContainer'>
                            <Button
                                type='submit'
                                size='small'
                                variant="outlined"
                                color='primary'
                            >
                                Сохранить
                            </Button>
                            <Button
                                size='small'
                                variant="outlined"
                                color='secondary'
                                onClick={() => setOpen(false)}
                            >
                                Закрыть
                            </Button>
                        </div>
                    </form>
                    : null
            }
        </div>
    )
};

export default AddNewProjectInMyCard;