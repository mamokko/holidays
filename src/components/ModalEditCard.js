import React, { useState } from 'react';
import Icon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";
import Modal from "@material-ui/core/Modal";
import Input from "./UI/Input";
import {IMaskInput} from "react-imask";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import * as firebase from "firebase";
import useForm from "react-hook-form";

const ModalEditCard = (props) => {

    const [open, setOpen] = useState(false);
    const {register, handleSubmit, errors} = useForm();
    const [departmentList] = useState(props.departmentList);
    const [postList] = useState(props.postList);

    const onSubmit = async (data) => {
        try {
            firebase.database().ref('/employee/' + props.id).update(data)
                .then(() => {
                    props.update();
                    setOpen(false)
                })
        } catch (e) {
            console.log(e);
        }
    };

    const renderPostItem = () => {
        if(postList) {
            return postList.map((item, index) => {
                return(
                    <option key={index}>{item.name}</option>
                )
            })
        }
    };
    const renderDepartmentItem = () => {
        if(departmentList) {
            return departmentList.map((item, index) => {
                return(
                    <option key={index}>{item.name}</option>
                )
            })
        }
    };

    return (
        <React.Fragment>
            <IconButton color='primary' onClick={() => setOpen(true)}>
                <Icon>create</Icon>
            </IconButton>
            <Modal open={open} onClose={() => setOpen(false)}>
                <form className='modal' onSubmit={handleSubmit(onSubmit)}>
                    <Input name='lastName' label='Фамилия' defaultValue={props.employeeItem.lastName} ref={register} />
                    <Input name='name' label='Имя' defaultValue={props.employeeItem.name} ref={register} />
                    <Input name='patronymic' label='Отчество' defaultValue={props.employeeItem.patronymic} ref={register} />
                    <div className='flexColumn' style={{marginBottom: '15px'}}>
                        <label className='label'>Отдел</label>
                        <Select
                            inputRef={register}
                            name='department'
                            style={{marginTop: '10px'}}
                            native
                        >
                            <option hidden>{props.employeeItem.department}</option>
                            { renderDepartmentItem() }
                        </Select>
                    </div>
                    <div className='flexColumn' style={{marginBottom: '15px'}}>
                        <label className='label'>Должность</label>
                        <Select
                            inputRef={register}
                            name='post'
                            style={{marginTop: '10px'}}
                            native
                        >
                            <option hidden>{props.employeeItem.post}</option>
                            { renderPostItem() }
                        </Select>
                    </div>
                    <div className='IMaskInputWrapper'>
                        <label className='label mb25'>Контактный телефон</label>
                        <IMaskInput
                            inputRef={register({minLength: 16})}
                            name='phone'
                            className='IMaskInput'
                            mask='+{7}(000)000-00-00'
                            label='Контактный телефон'
                            value={props.employeeItem.phone}
                        />
                        {errors.phone && errors.phone.type === 'minLength' && <p>Неверно введенное количество цифр </p>}
                    </div>
                    <div className='flexEnd' style={{marginTop: '15px'}}>
                        <Button type='submit' variant='contained' color='primary'>
                            Сохранить
                        </Button>
                        <Button
                            style={{marginLeft: 30}}
                            variant='contained'
                            color='primary'
                            onClick={() => setOpen(false)}
                        >
                            Отмена
                        </Button>
                    </div>
                </form>
            </Modal>
        </React.Fragment>
    )
};

export default ModalEditCard;