import React from 'react';

export default function CheckFilters(props) {
    return (
        <div className='flexStart'>
            <div className="filterBox" onClick={props.resetTime}>Сбросить время</div>
            {
                props.selectDepartment !== 'Все'
                    ?   <div className="filterBox" onClick={props.resetDepartment}>
                        <span className='mr15'>{props.selectDepartment}</span>
                        <span className="material-icons MuiIcon-root-96" aria-hidden="true">clear</span>
                    </div>
                    : null
            }
            {
                props.selectProject !== 'Все'
                    ?   <div className="filterBox" onClick={props.resetProject}>
                        <span className='mr15'>{props.selectProject}</span>
                        <span className="material-icons MuiIcon-root-96" aria-hidden="true">clear</span>
                    </div>
                    : null
            }
        </div>
    )
}