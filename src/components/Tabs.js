import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import EmployeeHolidays from './EmployeeHolidays';
import EmployeeProject from "./EmployeeProject";


function TabsMenu(props) {

    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => setValue(newValue);

    return (
        <div className='tabsContainer'>
            <AppBar position="static">
                <Tabs value={value} onChange={handleChange}>
                    <Tab label="Отпуска" />
                    <Tab label="Мои проекты" />
                    <Tab label="Подтверждающие отпуск" />
                </Tabs>
            </AppBar>
            {value === 0 && <EmployeeHolidays
                id={props.id}
                holidayList={props.holidayList}
                update={props.update}
            />}
            {value === 1 && <EmployeeProject id={props.id} />}
            {value === 2 && <span>Подтверждающие отпуска</span>}
        </div>
    );
}

export default TabsMenu;