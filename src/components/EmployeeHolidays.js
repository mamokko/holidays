import React, {useState} from 'react';
import ModalCreateHoliday from './ModalCreateHoliday';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import * as firebase from 'firebase';
import ModalEditHoliday from './ModalEditHoliday';
import axios from "../axios/axios";

export default function EmployeeHolidays(props) {

    const [holidayList, setHolidayList] = useState(props.holidayList);

    async function newHolidayList() {
        try {
            const response = await axios.get('/employee/' + props.id + '.json');
            setHolidayList(response.data.holidays);
        } catch(e) { console.log(e) }
    }

    const isDeleteHoliday = (index) => {
        firebase.database().ref('/employee/' + props.id + '/holidays/' + index).remove()
            .then(() => newHolidayList())
    };

    function renderBody() {
        return Object.keys(holidayList).map((items) => {
            const item = holidayList[items];
            const startData = new Date(item.selectedStartDay);
            const endData = new Date(item.selectedEndDay);
            const option = {
                day: 'numeric',
                month:'numeric',
                year: 'numeric'
            };
            return(
                <tr key={[items]} className='tableEmployeeBodyTr'>
                    <td className='tableEmployeeBodyTd'>{item.valueSelect}</td>
                    <td className='tableEmployeeBodyTd' style={{textAlign: 'center'}}>
                        с {startData.toLocaleString("ru", option)} по {endData.toLocaleString("ru", option)}
                    </td>
                    <td className='tableEmployeeBodyTd'>{item.comment}</td>
                    <td className='tableEmployeeBodyTdButton'>
                        <ModalEditHoliday
                            index={[items]}
                            id={props.id}
                            startData={startData.toLocaleString("ru", option)}
                            endData={endData.toLocaleString("ru", option)}
                            type={item.valueSelect}
                            comment={item.comment}
                            update={newHolidayList}
                        />
                        <IconButton color='primary' onClick={isDeleteHoliday.bind(this, [items])}>
                            <Icon>delete</Icon>
                        </IconButton>
                    </td>
                </tr>
            )
        })
    };

    return (
        <div className='tabContainer'>

            <ModalCreateHoliday id={props.id} update={props.update} />

            <table className='tableEmployee'>
                <thead>
                <tr className='tableEmployeeTr'>
                    <th className='tableEmployeeTh'><span>Тип отпуска</span></th>
                    <th className='tableEmployeeTh'><span>Даты отпуска</span></th>
                    <th className='tableEmployeeTh'><span>Комментарий</span></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                { renderBody() }

                </tbody>
            </table>
        </div>
    )
}
