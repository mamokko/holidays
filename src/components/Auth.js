import React, {Component} from 'react';
import {connect} from 'react-redux';
import Input from '../components/UI/Input';
import Button from '@material-ui/core/Button';
import ReactTooltip from 'react-tooltip';
import {isValid} from "../store/actions/action";
import '../App.css';


class Auth extends Component {

    state = {
        mail: '',
        pass: '',
        emailValid: false,
        passValid: false,
        formValid: false,
    };

    isValidForm = () => {
        if(this.state.emailValid && this.state.passValid) {
            this.setState({formValid: true})
        }
    };

    isValidEmail = (value) => {
        const re = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,4}$/;
        if(re.test(String(value).toLowerCase())) {
            this.setState({emailValid: true})
        }
        this.setState({mail:value});
        this.isValidForm();
    };

    isValidPass = (value) => {
        this.setState({pass:value});
        if(value.length >= 6) {
            this.setState({passValid: true})
        }
        this.isValidForm();
    };

    isAuthorization = () => {
        if(this.state.mail === 'ekaterina.volkova@dz.ru' && this.state.pass === '1234567') {
            this.props.isValid(true)
        }
    };

    render() {

        return(
            <div>
                <div>
                    <h1 className='caption'>Авторизация</h1>
                    <form className='authorizationForm'>

                        <Input label="Email" value={this.state.mail} onChange={(e) => this.isValidEmail(e.target.value)} />
                        <Input label="Пароль" value={this.state.pass} onChange={(e) => this.isValidPass(e.target.value)} />

                        <div>
                            <Button
                                variant='contained'
                                color='primary'
                                style={{marginRight: 20}}
                                disabled={!this.state.formValid}
                                onClick={() => {this.isAuthorization()}}
                            >
                                Войти
                            </Button>
                            <Button
                                variant='contained'
                                color='primary'
                                data-tip="Данная функция, на данный момент, недоступна"
                                data-for='disabledButton'
                            >
                                Регистрация
                            </Button>
                        </div>
                        <ReactTooltip id='disabledButton' />
                    </form>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        isValidData: state.isValidData,
    }
}

export default connect(mapStateToProps, {isValid})(Auth);