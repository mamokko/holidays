import React, {useEffect, useState} from 'react';
import axios from "../axios/axios";
import arrSort from './function/arrSort';
import AddNewProjectIn from './AddNewProjectInMyCard'
import Icon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";
import * as firebase from "firebase";

export default function EmployeeProject(props) {
    const [projectsList, setProjectsList] = useState([]);
    const [employeeProjects, setEmployeeProjects] = useState([]);

    async function receivingProjectList() {
        try {
            const response = await axios.get('/project.json');
            setProjectsList(arrSort(response.data, 'name'))
        } catch (e) {
            console.log(e)
        }
    }

    async function receivingData() {
        try {
            const response = await axios.get('/employee/' + props.id + '.json');
            const projects = Object.keys(response.data.project).map((key) => {
                const item = response.data.project[key];
                return {
                    key: key,
                    name: item.nameProject,
                };
            });
            setEmployeeProjects(arrSort(projects, 'name'))
        } catch (e) {
            console.log(e)
        }
    }

    useEffect(() => {
        receivingData();
        receivingProjectList();
    }, []);

    function  deleteHandler(index) {
        firebase.database().ref('/employee/' + props.id + '/project/' + index).remove()
            .then(() => {
                receivingData();
            })
    };

    const renderLi = () => {
        if(employeeProjects) {
            return Object.keys(employeeProjects).map((items) => {
                const item = employeeProjects[items];
                return(
                    <li key={item.key}>
                        <span> {item.name} </span>
                        <IconButton style={{padding: '5px'}}
                                    onClick={deleteHandler.bind(this, item.key)}
                                    color='primary'
                        >
                            <Icon>delete</Icon>
                        </IconButton>
                    </li>
                )
            })
        }
        return(
            <li> Проект отсутствует </li>
        )
    };

    return(
        <div className='tabContainer flex'>

            <AddNewProjectIn
                id={props.id}
                projectsList={projectsList}
                update={receivingData}
            />

            <div className='containerProjectsList'>
                <ul>
                    {renderLi()}
                </ul>
            </div>
        </div>
    )
}
