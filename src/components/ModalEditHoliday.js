import React, {useEffect, useState} from 'react';
import Modal from '@material-ui/core/Modal';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import MomentLocaleUtils, {formatDate, parseDate} from "react-day-picker/moment";
import PickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import 'moment/locale/ru';
import Input from './UI/Input';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import './UI/Modal.css';
import * as firebase from 'firebase';
import useForm from "react-hook-form";
import moment from 'moment';

const ModalEditHoliday = (props) => {

    const startData = moment(props.startData, "DD.MM.YYYY").format('YYYY-MM-DDTHH:mm:ss.SSSZ');
    const endData = moment(props.endData, "DD.MM.YYYY").format('YYYY-MM-DDTHH:mm:ss.SSSZ');

    const {register, handleSubmit, setValue} = useForm();
    const [open, setOpen] = useState(false);
    const [startDay, setStartDay] = useState(startData);
    const [endDay, setEndDay] = useState(endData);

    const isClose = () => setOpen(false);

    const onSubmit = async(data) => {
        try {
            firebase.database().ref('/employee/' + props.id + '/holidays/' + props.index).update(data)
                .then(() => props.update(props.id));
        } catch (e) {
            console.log(e)
        }
        setOpen(false);
    };

    const handleStartDayChange = (day) => setStartDay(day);
    const handleEndDayChange = (day) =>setEndDay(day);

    useEffect(() => {
        setValue('selectedStartDay', startDay);
        setValue('selectedEndDay', endDay);
        register({name: 'selectedStartDay'});
        register({name: 'selectedEndDay'});
    }, [register]);

    return(
        <React.Fragment>
            <IconButton color='primary' onClick={() => setOpen(true)}>
                <Icon>create</Icon>
            </IconButton>
            <Modal open={open} onClose={isClose}>
                <form className='modal' onSubmit={handleSubmit(onSubmit)}>
                    <div className='modalHeader'>
                        <h4 className='caption'>Редактирование отпуска</h4>
                        <IconButton color="primary" onClick={() => setOpen(false)}>
                            <Icon>close</Icon>
                        </IconButton>
                    </div>
                    <div className='flex-jcsa'>
                        <PickerInput
                            name='selectedStartDay'
                            value={props.startData}
                            formatDate={formatDate}
                            parseDate={parseDate}
                            inputProps={{ readOnly: true }}
                            dayPickerProps={{
                                locale: 'ru',
                                localeUtils: MomentLocaleUtils,
                            }}
                            onDayChange={handleStartDayChange}
                        />
                        <PickerInput
                            name='selectedEndDay'
                            value={props.endData}
                            formatDate={formatDate}
                            parseDate={parseDate}
                            inputProps={{ readOnly: true }}
                            dayPickerProps={{
                                locale: 'ru',
                                localeUtils: MomentLocaleUtils,
                            }}
                            onDayChange={handleEndDayChange}
                        />
                    </div>
                    <div className='flex-jcsa'>
                        <div className='flexColumn'>
                            <label className='label'>Тип отпуска</label>
                            <Select
                                name='valueSelect'
                                inputRef={register}
                                style={{width: 205, marginTop: 10}}
                                native
                            >
                                <option hidden>{props.type}</option>
                                <option>Очередной отпуск</option>
                                <option>Отгул</option>
                                <option>Отпуск за свой счет</option>
                                <option>Декретный отпуск</option>
                                <option>Конференция, обучение</option>
                            </Select>
                        </div>
                        <Input
                            name='comment' ref={register}
                            label='Комментарий'
                            defaultValue={props.comment}
                        />
                    </div>
                    <div className='flexEnd'>
                        <Button type='submit' variant='contained' color='primary' style={{marginRight: 30}}>
                            Сохранить
                        </Button>
                        <Button variant='contained' color='primary' onClick={() => setOpen(false)}>
                            Отмена
                        </Button>
                    </div>
                </form>
            </Modal>
        </React.Fragment>
    )
};

export default ModalEditHoliday;