import React from 'react';
import Input from '@material-ui/core/Input';
import '../../App.css';

const DefaultInput = React.forwardRef((props, ref) => {

    const inputType = props.type || 'text';
    const htmlFor = `${inputType}-${Math.random()}`;

    return(
        <div style={{marginBottom: 15, display: 'flex', flexDirection: 'column', alignItems: 'start', marginRight: '10px'}}>
            <label htmlFor={htmlFor} className='label'>{props.label}</label>
            <Input
                type={inputType}
                id={htmlFor}
                value={props.value}
                onChange={props.onChange}
                className='input'
                defaultValue={props.defaultValue}
                inputRef={ref}
                name={props.name}
            />

        </div>
    )
});

export default DefaultInput;