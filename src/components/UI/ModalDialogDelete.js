import React, {useState} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import Icon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';

export default function ModalDialogDelete(props) {

    const [open, setOpen] = useState(false);

    const onClick = () => {
        props.delete();
        setOpen(false);
    };

    return (
        <div>
            <IconButton color='primary' onClick={() => setOpen(true)}>
                <Icon>delete</Icon>
            </IconButton>
            <Dialog
                open={open}
                onClose={() => setOpen(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle>{'Вы действительно хотите удалить данный элемент?'}</DialogTitle>
                <DialogActions>
                    <Button onClick={onClick} color="primary">
                        Да
                    </Button>
                    <Button onClick={() => setOpen(false)} color="primary" autoFocus>
                        Отмена
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}