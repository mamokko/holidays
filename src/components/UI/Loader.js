import React, {useState} from 'react';
import CircleLoader from 'react-spinners/CircleLoader';

const override = {
    display: 'block',
    margin: '20vh auto',
    borderColor: 'red',
};

export default function Loader() {
    const [loading] = useState(true);
    return (
        <div className='sweet-loading'>
            <CircleLoader
                css={override}
                sizeUnit={"px"}
                size={150}
                color={'#00796B'}
                loading={loading}
            />
        </div>
    )
}
