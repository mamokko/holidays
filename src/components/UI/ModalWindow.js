import React from 'react';
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";

const ModalWindow = (props) => {
    return (

        <Modal
            open={props.open}
            onClose={props.close}
        >
            <form className='modal'>

                {props.children}

                <div className='flexEnd'>
                    <Button
                        style={{marginRight: 30}}
                        variant='contained'
                        color='primary'
                        onClick={props.editHandler}
                    >
                        Сохранить
                    </Button>
                    <Button
                        variant='contained'
                        color='primary'
                        onClick={props.close}
                    >
                        Отмена
                    </Button>
                </div>
            </form>
        </Modal>

    )
};

export default ModalWindow;