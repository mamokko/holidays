import React, {useState} from 'react';
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import * as firebase from 'firebase';
import ModalDialogDelete from "./ModalDialogDelete";

const SectionItem = (props) => {

    const [edit, setEdit] = useState(false);
    const [readOnly, setReadOnly] = useState(true);

    const onClickEditHandler = () => {
        setEdit(true);
        setReadOnly(false);
        document.getElementById(`${props.id}`).focus();
    };
    const onDelete = () => firebase.database().ref(`${props.name}/` + props.id).remove().then(() => props.update());
    const onClickCheck = () => {
        const newName = document.getElementById(`${props.id}`).value;
        console.log(newName);
        if(newName) {
            firebase.database().ref(`${props.name}/` + props.id).set({
                name: newName
            }).then(() => {
                setReadOnly(true);
                setEdit(false);
            })
        }
    };
    const onClickCloseHandler = () => setEdit(false);

    return (
        <div className='directoryLiItem'>
            <input
                readOnly={readOnly}
                defaultValue={props.item.name}
                id={props.id}
                // onBlur={() => setEdit(false)}
            />
            {
                !edit
                    ?
                    <React.Fragment>
                        <IconButton color='primary' onClick={onClickEditHandler}>
                            <Icon>edit</Icon>
                        </IconButton>
                        <ModalDialogDelete delete={onDelete} clear={props.clear} />
                    </React.Fragment>
                    :
                    <React.Fragment>
                        <IconButton color='primary' onClick={onClickCheck}>
                            <Icon>check</Icon>
                        </IconButton>
                        <IconButton color='primary' onClick={onClickCloseHandler}>
                            <Icon>clear</Icon>
                        </IconButton>
                    </React.Fragment>
            }
        </div>
    )
};

export default SectionItem;