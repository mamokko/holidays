import React from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import './Modal.css';
import '../../App.css';

const PickerInput = props => {

    const isInvalid = ({valid, touched, shouldValidate}) => {
        return !valid && shouldValidate && touched
    };

    const htmlFor = `'DayPickerInput'-${Math.random()}`;

    return(
        <div className='flexColumn'>
            <label htmlFor={htmlFor} className='label'>{props.label}</label>
            <DayPickerInput
                placeholder={props.placeholder}
                id={htmlFor}
                onDayChange={props.onDayChange}
            />
            {isInvalid(props)
                ? <span>Поле не должно быть пустым</span>
                : null
            }
        </div>
    )
};

export default PickerInput;