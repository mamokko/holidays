import React from 'react';
import PickerInput from "react-day-picker/DayPickerInput";
import MomentLocaleUtils, {formatDate, parseDate} from "react-day-picker/moment";
import Button from "@material-ui/core/Button";

export default function FilterTime(props) {

    return (
        <div>
            <h3 className='caption'>Временные фильтры</h3>
            <div className='summaryCalendar__wrapper'>
                <PickerInput
                    formatDate={formatDate}
                    parseDate={parseDate}
                    placeholder={`${formatDate(new Date(), 'DD.MM.YYYY')}`}
                    inputProps={{ readOnly: true }}
                    dayPickerProps={{
                        locale: 'ru',
                        localeUtils: MomentLocaleUtils,
                    }}
                    onDayChange={props.startDayChange}
                />
                <PickerInput
                    formatDate={formatDate}
                    parseDate={parseDate}
                    placeholder={`${formatDate(new Date(), 'DD.MM.YYYY')}`}
                    inputProps={{ readOnly: true }}
                    dayPickerProps={{
                        locale: 'ru',
                        localeUtils: MomentLocaleUtils,
                        // disabledDays: {before: new Date(this.state.filterStartDate)}
                    }}
                    onDayChange={props.endDayChange}
                />
                <Button
                    color='primary'
                    variant='contained'
                    onClick={props.renderThisDates}
                > OK </Button>
            </div>
            <Button
                color='primary'
                variant='contained'
                onClick={props.renderThisYear}
                style={{marginRight: 15}}
            >
                Весь год
            </Button>
            <Button
                variant='contained'
                color='primary'
                onClick={props.renderThisMonth}
            >
                Этот месяц
            </Button>
        </div>
    )
}