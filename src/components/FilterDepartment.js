import React from 'react';
import Select from "@material-ui/core/Select";

export default function FilterDepartment(props) {

    function renderDepartmentsItem() {
        return Object.keys(props.departments).map((items, index) => {
            const item = props.departments[items];
            return(
                <option key={index}>
                    {item.name}
                </option>
            )
        })
    };

    return (
        <div className='flexColumn'>
            <h3 className='caption'>Фильтры по отделам</h3>
            {props.selectDepartment === 'Все'
                ?
                <Select
                    native
                    onChange={props.changeDepartment}
                    style={{marginTop: 10}}
                    value='Все'
                >
                    <option>Все</option>
                    { props.departments ? renderDepartmentsItem() : null }
                </Select>
                :
                <Select
                    native
                    onChange={props.changeDepartment}
                    style={{marginTop: 10}}
                >
                    <option>{props.selectDepartment}</option>
                    { props.departments ? renderDepartmentsItem() : null }
                </Select>
            }
        </div>
    )
}