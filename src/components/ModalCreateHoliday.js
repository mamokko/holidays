import React, {useEffect, useState} from 'react';
import Modal from '@material-ui/core/Modal';
import useForm from "react-hook-form";
import PickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment';
import 'moment/locale/ru';
import './UI/Modal.css';
import Input from './UI/Input';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import axios from "../axios/axios";

const ModalCreateHoliday = (props) => {

    const {register, handleSubmit, setValue, errors} = useForm();
    const [open, setOpen] = useState(false);

    const isClose = () => setOpen(false);

    const onSubmit = async(data) => {
        try {
            await axios.post('/employee/' + props.id + '/holidays.json', data)
                .then(() => {props.update()})
        } catch (e) {
            console.log(e)
        }
        setOpen(false);
    };

    const handleStartDayChange = (day) => setValue('selectedStartDay', day);
    const handleEndDayChange = (day) => setValue('selectedEndDay', day);

    useEffect(() => {
        register({name: 'selectedStartDay'}, {required: true});
        register({name: 'selectedEndDay'}, {required: true});
    }, [register]);

    return (
        <React.Fragment>
            <Button variant='contained' color='primary' onClick={() => setOpen(true)}>
                Запланировать отпуск
            </Button>
            <Modal open={open} onClose={isClose}>
                <form className='modal' onSubmit={handleSubmit(onSubmit)}>
                    <div className='modalHeader'>
                        <h4 className='caption'>Добавление отпуска</h4>
                    </div>
                    <div className='flex-jcsa'>
                        <div>
                            <PickerInput
                                name='selectedStartDay'
                                formatDate={formatDate}
                                parseDate={parseDate}
                                placeholder={`${formatDate(new Date(), 'DD.MM.YYYY')}`}
                                inputProps={{ readOnly: true }}
                                dayPickerProps={{
                                    locale: 'ru',
                                    localeUtils: MomentLocaleUtils,
                                    // disabledDays: {
                                    //     before: new Date(),
                                    //     after: this.state.selectedEndDay
                                    // }
                                }}
                                onDayChange={handleStartDayChange}
                            />
                            {errors.selectedStartDay && <p>Это поле не должно быть пустым</p>}
                        </div>
                        <div>
                            <PickerInput
                                name='selectedEndDay'
                                formatDate={formatDate}
                                parseDate={parseDate}
                                placeholder={`${formatDate(new Date(), 'DD.MM.YYYY')}`}
                                inputProps={{ readOnly: true }}
                                dayPickerProps={{
                                    locale: 'ru',
                                    localeUtils: MomentLocaleUtils,
                                    // disabledDays: {before: new Date(this.state.selectedStartDay || new Date())}
                                }}
                                onDayChange={handleEndDayChange}
                            />
                            {errors.selectedEndDay && <p>Это поле не должно быть пустым</p>}
                        </div>
                    </div>
                    <div className='flex-jcsa'>
                        <div className='flexColumn'>
                            <label className='label'>Тип отпуска *</label>
                            <Select
                                name='valueSelect'
                                inputRef={register({required: true})}
                                style={{width: 205, marginTop: 10}}
                                native
                            >
                                <option value='' hidden>Выберите тип отпуска</option>
                                <option>Очередной отпуск</option>
                                <option>Отгул</option>
                                <option>Отпуск за свой счет</option>
                                <option>Декретный отпуск</option>
                                <option>Конференция, обучение</option>
                            </Select>
                            {errors.valueSelect && <p>Это поле не должно быть пустым</p>}
                        </div>
                        <Input name='comment' ref={register} label='Комментарий' />
                    </div>
                    <div className='flexEnd'>
                        <Button style={{marginRight: 30}} variant='contained' color='primary' type='submit'>
                            Добавить
                        </Button>
                        <Button variant='contained' color='primary' onClick={() => setOpen(false)}>
                            Отмена
                        </Button>
                    </div>
                </form>
            </Modal>
        </React.Fragment>
    )
};

export default ModalCreateHoliday;