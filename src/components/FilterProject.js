import React from 'react';
import Select from "@material-ui/core/Select";

export default function FilterProject(props) {

    function renderProjectsItem() {
        return Object.keys(props.projects).map((items, index) => {
            const item = props.projects[items];
            return(
                <option key={index}>
                    {item.name}
                </option>
            )
        })
    };

    return (
        <div className='flexColumn'>
            <h3 className='caption'>Фильтры по проектам</h3>
            {props.selectProject ==='Все'
                ?
                <Select
                    native
                    style={{marginTop: 10}}
                    onChange={props.changeProject}
                    value='Все'
                >
                    <option>Все</option>
                    { props.projects ? renderProjectsItem() : null }
                </Select>
                :
                <Select
                    native
                    style={{marginTop: 10}}
                    onChange={props.changeProject}
                >
                    <option>{props.selectProject}</option>
                    { props.projects ? renderProjectsItem() : null }
                </Select>
            }

        </div>
    )
}