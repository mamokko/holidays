import React from 'react';
import SectionList from '../containers/SectionList';
import '../App.css';

const Directory = () => {
        return(
        <div>
            <h1 className='caption'>Справочники</h1>

            <div className='directoryContainer'>

                <SectionList name='department' label='Отделы' />

                <SectionList name='postList' label='Должности' />

                <SectionList name='project' label='Проекты' />

            </div>
        </div>
    )
};

export default Directory;