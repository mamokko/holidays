import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NavLink} from 'react-router-dom';
import Button from '@material-ui/core/Button';
import '../App.css';
import {isValid} from "../store/actions/action";

const links = [
    {to: '/', label: 'Список Сотрудников'},
    {to: '/summaryCalendar', label: 'Сводный календарь'},
    {to: '/directory', label: 'Справочники'},
    // {to: '/auth', label: 'Войти', }
];

class Navigation extends Component {

    renderLinks() {
        return links.map((link, index) => {
            return(
                <li
                    className='navLi'
                    key={index}>
                    <NavLink
                        className='navLink'
                        to={link.to}
                    >
                        {link.label}
                    </NavLink>
                </li>
            )
        })
    }

    isLogout = () => {
        this.props.isValid(false)
    };

    render() {

        return(
            <nav className='nav'>
                <ul className='navUl'>
                    {this.renderLinks()}
                </ul>
                <Button
                    variant='contained'
                    color='secondary'
                    style={{margin: '10px 10px 10px -100px'}}
                    onClick={() => {this.isLogout()}}
                >
                    Выйти
                </Button>
            </nav>
        )
    }
}

export default connect(null, {isValid})(Navigation);