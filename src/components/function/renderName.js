import React from "react";

export default function renderName(selectDepartment, selectProject, employees) {
    if(selectDepartment === 'Все' && selectProject === 'Все') {
        return employees.map((item, index) => {
            return(
                <div key={index} className='tableName'> {item.lastName + ' ' + item.name} </div>
            )
        });
    }
    if(selectDepartment !== 'Все' && selectProject === 'Все') {
        return employees.map((item) => {

            let employees = [];
            if(selectDepartment === item.department) {
                employees.push(item);
            }
            if(employees.length > 0) {
                return employees.map((element, i) =>{
                    return(
                        <div className='tableName' key={i}> {element.lastName + ' ' + element.name} </div>
                    )
                })
            } else {
                return null;
            }

        })
    }
    if(selectDepartment === 'Все' && selectProject !== 'Все') {
        return employees.map((item) => {
            if(item.project) {
                return Object.keys(item.project).map((data) => {
                    const itemProject = item.project[data];
                    let employees = [];
                    if(selectProject === itemProject.nameProject) {
                        employees.push(item);
                    }
                    if(employees.length > 0) {
                        return employees.map((element, i) =>{
                            return(
                                <div className='tableName' key={i}> {element.lastName + ' ' + element.name} </div>
                            )
                        })
                    } else {
                        return null;
                    }
                });
            } else {
                return null;
            }
        })
    }
    if(selectDepartment !== 'Все' && selectProject !== 'Все') {
        return employees.map((item) => {
            if(item.project) {
                return Object.keys(item.project).map((data) => {
                    const itemProject = item.project[data];

                    let employees = [];
                    if(selectProject === itemProject.nameProject && selectDepartment === item.department) {
                        employees.push(item);
                    }
                    if(employees.length > 0) {
                        return employees.map((element, i) =>{
                            return(
                                <div key={i} className='tableName'> {element.lastName + ' ' + element.name} </div>
                            )
                        })
                    } else {
                        return null;
                    }
                })
            } else {
                return null;
            }
        })
    }
}