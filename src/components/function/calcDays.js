import moment from 'moment';

export default function calcDays(startSchedulerDate, endSchedulerDate) {
    moment.locale('ru');
    let startDate = moment(startSchedulerDate);
    let startMonthDay = startDate.date();
    let endDate = moment(endSchedulerDate);
    let month = startDate.month();
    let monthCol = 0;
    let result = [];
    for(let currentDate = startDate; currentDate <= endDate; currentDate.add(1, 'days')) {
        if(currentDate.month() !== month) {
            let lastMonth = moment().month(month);
            result.push({month: lastMonth.format('MMMM'), days: monthCol, start: startMonthDay});
            month = currentDate.month();
            startMonthDay = 1;
            monthCol = 0;
        }
        monthCol++;
    }
    let lastMonth = endDate.month();
    if(endDate.subtract(1, 'd').month() === lastMonth){
        result.push({month: endDate.format('MMMM'), days: monthCol, start: startMonthDay});
    }
    return result;
}