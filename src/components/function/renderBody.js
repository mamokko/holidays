import React from "react";

export default function renderBody(selectDepartment, selectProject, employees, renderColorDays) {
    if(selectDepartment === 'Все' && selectProject === 'Все') {
        return employees.map((item, index) => {
            return(
                <tr key={index}>
                    {renderColorDays(item)}
                </tr>
            )
        });
    }
    if(selectDepartment !== 'Все' && selectProject === 'Все') {
        return employees.map((item) => {

            let employees = [];
            if(selectDepartment === item.department) {
                employees.push(item);
            }
            if(employees.length > 0) {
                return employees.map((element, i) => {
                    return(
                        <tr key={i}>
                            {renderColorDays(element)}
                        </tr>
                    )
                })
            } else {
                return null;
            }
        })
    }
    if(selectDepartment === 'Все' && selectProject !== 'Все') {

        return employees.map((item) => {
            if(item.project) {
                return Object.keys(item.project).map((data) => {
                    const itemProject = item.project[data];

                    let employees = [];
                    if(selectProject === itemProject.nameProject) {
                        employees.push(item);
                    }
                    if(employees.length > 0) {
                        return employees.map((element, i) =>{
                            return(
                                <tr key={i}>
                                    {renderColorDays(element)}
                                </tr>
                            )
                        })
                    } else {
                        return null;
                    }
                });
            } else {
                return null;
            }
        })
    }
    if(selectDepartment !== 'Все' && selectProject !== 'Все') {
        return employees.map((item) => {
            if(item.project) {
                return Object.keys(item.project).map((data) => {
                    const itemProject = item.project[data];

                    let employees = [];
                    if(selectProject === itemProject.nameProject && selectDepartment === item.department) {
                        employees.push(item);
                    }
                    if(employees.length > 0) {
                        return employees.map((element, i) =>{
                            return(
                                <tr key={i}>
                                    {renderColorDays(element)}
                                </tr>
                            )
                        })
                    } else {
                        return null;
                    }
                })
            } else {
                return null;
            }
        })
    }
}