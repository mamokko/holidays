export default function arrSort(object, param = null) {

    const newArr = Object.keys(object).map((item) => object[item]);

    if(!param) {
        return newArr;
    }

    newArr.sort((a, b) => {
        if(a[param] > b[param]) {
            return 1;
        }
        if(a[param] < b[param]) {
            return -1;
        }
        return 0;
    });

    return newArr;
}